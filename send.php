<?php
include __DIR__ . '/src/bootstrap.php';
$config = include __DIR__ . '/src/config/config.php';

(new App($config))->run();
