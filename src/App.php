<?php

use models\Track;

class App
{
    public static array $config;

    public function __construct($config)
    {
        static::$config = $config;
    }

    public function run(): void
    {
        foreach (static::$config['playingServices'] as $serviceName) {
            /** @var Track|null $track */
            if ($track = (new $serviceName())->getTrack()) {
                break;
            }
        }

        if ($track === null) {
            echo "No playing tracks\n";

            return;
        }

        foreach (static::$config['infoServices'] as $serviceName) {
            if ($track->loadInfo($serviceName)) {
                break;
            }
        }

        printf(
            "Track: %s\nAlbum: %s\nImage: %s\nURL: %s\n\n",
            $track->getFormattedName(),
            $track->album,
            $track->image,
            $track->url
        );

        foreach (static::$config['postingServices'] as $serviceName) {
            $service = new $serviceName($track);
            $serviceName = (new ReflectionClass($service))->getShortName();

            printf('Post to %s? [y/n][n] ', $serviceName);

            $fh = fopen('php://stdin', 'rb');

            if (trim(fgets($fh)) === 'y') {
                printf(
                    "%s\n",
                    $service->post() ? 'Sent' : 'Not sent'
                );
            }

            fclose($fh);
        }
    }
}
