<?php

use services\info\Spotify;
use services\info\YandexMusic;
use services\playing\Lastfm;
use services\posting\Twitter;

$config = [
    'playingServices' => [
        Lastfm::class,
    ],
    'infoServices' => [
        Spotify::class,
        YandexMusic::class,
    ],
    'postingServices' => [
        Twitter::class,
    ],
    Lastfm::class => [
        'user' => '',
        'key' => '',
    ],
    Spotify::class => [
        'client_id' => '',
        'client_secret' => '',
    ],
    Twitter::class => [
        'consumer_key' => '',
        'consumer_secret' => '',
        'token' => '',
        'token_secret' => '',
    ],
];

if (file_exists(__DIR__ . '/config-local.php')) {
    $config = array_merge(
        $config,
        include __DIR__ . '/config-local.php'
    );
}

return $config;
