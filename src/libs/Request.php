<?php

namespace libs;

class Request
{
    private $handler;
    private $error = [];

    public function __construct()
    {
        $this->handler = curl_init();
    }

    public function __destruct()
    {
        curl_close($this->handler);
    }

    public function get($path = null, $params = [])
    {
        if (!empty($params['data'])) {
            $params['queryString'] = $this->buildParamsString($params['data']);

            unset($params['data']);
        }

        return $this->request($path, $params);
    }

    public function post($path = null, $params = [])
    {
        curl_setopt($this->handler, CURLOPT_POST, true);

        if (!empty($params['data'])) {
            if (isset($params['headers'])) {
                $contentTypeHeader = array_values(
                    array_filter(
                        $params['headers'],
                        static fn($value, $key) => strtolower($key) === 'content-type',
                        ARRAY_FILTER_USE_BOTH
                    )
                )[0] ?? '';

                if ($contentTypeHeader === 'application/x-www-form-urlencoded') {
                    $data = http_build_query($params['data']);
                } else {
                    $data = $params['data'];
                }
            }

            curl_setopt($this->handler, CURLOPT_POSTFIELDS, $data);
        }

        return $this->request($path, $params);
    }

    public function head($path = null, $params = [])
    {
        curl_setopt($this->handler, CURLOPT_CUSTOMREQUEST, 'HEAD');
        curl_setopt($this->handler, CURLOPT_HEADER, true);

        if (!empty($params['data'])) {
            $params['queryString'] = $this->buildParamsString($params['data']);
            unset($params['data']);
        }

        return $this->request($path, $params);
    }

    public function put($path = null, $params = [])
    {
        curl_setopt($this->handler, CURLOPT_CUSTOMREQUEST, 'PUT');

        if (!empty($params['data'])) {
            curl_setopt($this->handler, CURLOPT_POSTFIELDS, $params['data']);
        }

        return $this->request($path, $params);
    }

    public function delete($path = null, $params = [])
    {
        curl_setopt($this->handler, CURLOPT_CUSTOMREQUEST, 'DELETE');

        return $this->request($path, $params);
    }

    public function errorInfo($raw = true)
    {
        return $raw ? $this->error : sprintf('%s: %s', $this->error['errno'], $this->error['text']);
    }

    private function request($path, $params)
    {
        if (empty($path)) {
            return false;
        }

        if (!empty($params['queryString'])) {
            $path = implode('?', [$path, $params['queryString']]);
        }

        $requestOptions = [
            CURLINFO_HEADER_OUT => true,
            CURLOPT_HEADER => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $path
        ];

        if (!empty($params['headers'])) {
            if (!empty($params['headers']['User-Agent'])) {
                $requestOptions[CURLOPT_USERAGENT] = $params['headers']['User-Agent'];
                unset($params['headers']['User-Agent']);
            }

            if (!empty($params['headers']['Referer'])) {
                $requestOptions[CURLOPT_REFERER] = $params['headers']['Referer'];
                unset($params['headers']['Referer']);
            }

            foreach ($params['headers'] as $name => $value) {
                $requestOptions[CURLOPT_HTTPHEADER][] = sprintf('%s: %s', $name, $value);
            }
        }

        if (!empty($params['cookie'])) {
            $requestOptions[CURLOPT_COOKIE] = $this->buildParamsString($params['cookie'], '; ');
        }

        curl_setopt_array($this->handler, $requestOptions);

        $data = curl_exec($this->handler);

        if (curl_errno($this->handler)) {
            $this->error['errno'] = curl_errno($this->handler);
            $this->error['text'] = curl_error($this->handler);

            return null;
        }

        $info = curl_getinfo($this->handler);
        $headers = substr($data, 0, $info['header_size']);

        return [
            'url' => $info['url'],
            'code' => $info['http_code'],
            'status' => strstr($headers, "\r\n", true),
            'requestHeaders' => $this->parseHeaders($info['request_header']),
            'responseHeaders' => $this->parseHeaders($headers),
            'result' => substr($data, $info['header_size']),
        ];
    }

    private function parseHeaders($headers)
    {
        $data = [];
        $headers = trim($headers);

        foreach (explode("\r\n", $headers) as $header) {
            if (strpos($header, ':')) {
                list($key, $value) = explode(': ', $header);
                $data[$key] = $value;
            }
        }

        return $data;
    }

    private function buildParamsString($data = [], $separator = '&')
    {
        return http_build_query($data, '', $separator, PHP_QUERY_RFC3986);
    }
}
