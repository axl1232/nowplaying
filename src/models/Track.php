<?php

namespace models;

use RuntimeException;

class Track
{
    public string $artist;
    public string $title;
    public ?string $album = null;
    public ?string $url = null;
    public ?string $image = null;

    public function __construct($artist, $title)
    {
        $this->artist = $artist;
        $this->title = $title;
    }

    public function loadInfo(string $service): bool
    {
        if (empty($service) || !class_exists($service)) {
            return false;
        }

        try {
            if ($info = (new $service())->getTrackInfo($this->artist, $this->title)) {
                $this->album = $info->album;
                $this->url = $info->url;
                $this->image = $info->image;

                return true;
            }
        } catch (RuntimeException $e) {
            return false;
        }

        return false;
    }

    public function getFormattedName(): string
    {
        return sprintf('%s - %s', $this->artist, $this->title);
    }
}
