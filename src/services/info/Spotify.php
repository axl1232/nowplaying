<?php

namespace services\info;

use App;
use RuntimeException;
use JsonException;
use libs\Request;
use models\Track;

class Spotify implements TrackInfo
{
    private string $tokenType;
    private string $token;

    public function __construct()
    {
        $this->getToken();
    }

    public function getTrackInfo(string $artist, string $title, ?string $album = null): Track
    {
        $result = (new Request())->get(
            'https://api.spotify.com/v1/search',
            [
                'headers' => [
                    'Authorization' => "{$this->tokenType} {$this->token}",
                ],
                'data' => [
                    'q' => "artist:{$artist} track:{$title}" . ($album ? " album:{$album}" : ''),
                    'type' => 'track',
                    'market' => 'RU',
                    'offset' => 0,
                    'limit' => 10,
                ],
            ]
        );

        if ($result['code'] === 200) {
            try {
                $data = json_decode($result['result'], true, 512, JSON_THROW_ON_ERROR);
                if (!empty($data['tracks']['items'])) {
                    $item = $data['tracks']['items'][0];

                    $track = new Track($item['artists'][0]['name'], $item['name']);
                    $track->album = $item['album']['name'];
                    $track->url = $item['external_urls']['spotify'];
                    $track->image = $item['album']['images'][0]['url'];

                    return $track;
                }
            } catch (JsonException $e) {
                throw new RuntimeException('Unable to parse track info');
            }
        }

        throw new RuntimeException('Unable to find track info');
    }

    private function getToken(): void
    {
        $result = (new Request())->post(
            'https://accounts.spotify.com/api/token',
            [
                'headers' => [
                    'Authorization' => 'Basic ' . base64_encode(
                        sprintf(
                            '%s:%s',
                            App::$config[static::class]['client_id'],
                            App::$config[static::class]['client_secret']
                        )
                    ),
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'data' => [
                    'grant_type' => 'client_credentials',
                ],
            ]
        );

        if ($result['code'] !== 200) {
            throw new RuntimeException('Unable to get access token');
        }

        try {
            $data = json_decode($result['result'], true, 512, JSON_THROW_ON_ERROR);
            $this->token = $data['access_token'];
            $this->tokenType = $data['token_type'];
        } catch (JsonException $e) {
            throw new RuntimeException('Unable to parse access token');
        }
    }
}
