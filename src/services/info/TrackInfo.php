<?php

namespace services\info;

use models\Track;

interface TrackInfo
{
    public function getTrackInfo(string $artist, string $title, ?string $album = null): Track;
}
