<?php

namespace services\info;

use JsonException;
use libs\Request;
use models\Track;
use RuntimeException;

class YandexMusic implements TrackInfo
{
    public function getTrackInfo(string $artist, string $title, ?string $album = null): Track
    {
        $result = (new Request())->get(
            'https://music.yandex.ru/handlers/music-search.jsx',
            [
                'data' => [
                    'text' => "{$artist} - {$title}",
                    'type' => 'tracks',
                ],
                'cookies' => [
                    'yandexuid' => '19965846614874555830',
                ],
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 8.1; InfoPath.2; .NET CLR 2.8.30730; WOW64; Trident/6.0)',
                    'Referer' => 'music.yandex.ru',
                ],
            ]
        );

        if ($result['code'] === 200) {
            try {
                $result = json_decode($result['result'], true, 512, JSON_THROW_ON_ERROR);

                if (!empty($result['tracks']['items'])) {
                    foreach ($result['tracks']['items'] as $item) {
                        if (
                            $item['available']
                            && $this->isEqual($title, $item['title'])
                            && $this->isEqual($artist, $item['artists'][0]['name'])
                            // && $this->isEqual($track->getAlbum(), $item['albums'][0]['title'])
                        ) {
                            $track = new Track($item['title'], $item['artists'][0]['name']);
                            $track->album = $item['albums'][0]['title'];
                            $track->url = "https://music.yandex.ru/album/{$item['albums'][0]['id']}/track/{$item['id']}";

                            if (!empty($item['albums'][0]['coverUri'])) {
                                $track->image = str_replace('%%', '400x400', "https://{$item['albums'][0]['coverUri']}");
                            }

                            return $track;
                        }
                    }
                }
            } catch (JsonException $e) {
                throw new RuntimeException('Unable to parse track info');
            }
        }

        throw new RuntimeException('Unable to find track info');
    }

    public function isEqual($string1, $string2): bool
    {
        $strings = array_map(
            'mb_strtolower',
            [$string1, $string2]
        );

        $result = 0;
        similar_text($strings[0], $strings[1], $result);

        return $result > 80;
    }
}
