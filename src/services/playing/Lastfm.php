<?php

namespace services\playing;

use App;
use libs\Request;
use models\Track;

class Lastfm implements Playing
{
    public function getTrack(): ?Track
    {
        $data = (new Request())->get(
            'http://ws.audioscrobbler.com/2.0/',
            [
                'data' => [
                    'method' => 'user.getrecenttracks',
                    'user' => App::$config[static::class]['user'],
                    'limit' => 1,
                    'api_key' => App::$config[static::class]['key'],
                    'format' => 'json',
                ],
            ]
        );

        if ($data['code'] !== 200) {
            // throw new RuntimeException('Unable to get current track');
            return null;
        }

        $trackData = json_decode($data['result'], true);

        if (!empty($trackData['recenttracks']['track'][0])) {
            $data = $trackData['recenttracks']['track'][0];

            if (
                !empty($data['@attr']['nowplaying']) &&
                $data['@attr']['nowplaying'] === 'true' &&
                !empty($data['artist']['#text']) &&
                !empty($data['name'])
            ) {
                $track = new Track($data['artist']['#text'], $data['name']);

                if (!empty($data['album']['#text'])) {
                    $track->album = $data['album']['#text'];
                }

                return $track;
            }
        }

        return null;
    }
}
