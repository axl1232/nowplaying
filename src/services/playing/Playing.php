<?php

namespace services\playing;

interface Playing
{
    public function getTrack();
}
