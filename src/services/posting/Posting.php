<?php

namespace services\posting;

use models\Track;

interface Posting
{
    public function __construct(Track $track);
    public function post(): bool;
}
