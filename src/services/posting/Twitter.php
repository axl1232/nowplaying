<?php

namespace services\posting;

use App;
use Exception;
use JsonException;
use libs\Request;
use models\Track;
use RuntimeException;

class Twitter implements Posting
{
    private Track $track;
    private array $tags = ['nowplaying', 'музыка'];

    public function __construct(Track $track)
    {
        $this->track = $track;
    }

    public function post(): bool
    {
        $statusUrl = 'https://api.twitter.com/1.1/statuses/update.json';
        $statusParams = $this->getRequestParams();
        $statusParams['status'] = $this->formatPost($this->track);

        if ($this->track->image) {
            $mediaUrl = 'https://upload.twitter.com/1.1/media/upload.json';
            $mediaParams = $this->getRequestParams();
            $mediaParams['media_data'] = base64_encode(file_get_contents($this->track->image));
            $mediaParams['oauth_signature'] = $this->getSignature('POST', $mediaUrl, $mediaParams);

            ksort($mediaParams);

            $result = (new Request())->post(
                $mediaUrl,
                [
                    'data' => http_build_query($mediaParams, '', '&', PHP_QUERY_RFC3986),
                    'headers' => [
                        'Authorization' => $this->getAuthorizationHeader($mediaParams),
                    ]
                ]
            );

            if ($result['code'] !== 200) {
                return false;
            }

            try {
                $result = json_decode($result['result'], true, 512, JSON_THROW_ON_ERROR);

                if (!empty($result['media_id_string'])) {
                    $statusParams['media_ids'] = $result['media_id_string'];
                }
            } catch (JsonException $e) {
                throw new RuntimeException('Unable to upload media');
            }
        }

        $statusParams['oauth_signature'] = $this->getSignature('POST', $statusUrl, $statusParams);

        ksort($statusParams);

        $result = (new Request())->post(
            $statusUrl,
            [
                'data' => http_build_query($statusParams, '', '&', PHP_QUERY_RFC3986),
                'headers' => [
                    'Authorization' => $this->getAuthorizationHeader($statusParams),
                ]
            ]
        );

        if ($result['code'] !== 200) {
            throw new RuntimeException('Unable to send post');
        }

        return true;
    }

    private function formatPost(Track $track): string
    {
        return sprintf(
            'Now playing: %s %s',
            $track->getFormattedName() . (empty($track->url) ? '' : " {$track->url}"),
            implode(
                ' ',
                array_map(
                    static fn($tag) => "#{$tag}",
                    $this->tags
                )
            )
        );
    }

    private function getRequestParams(): array
    {
        try {
            return [
                'include_entities' => 'true',
                'oauth_consumer_key' => App::$config[static::class]['consumer_key'],
                'oauth_nonce' => md5(random_bytes(32)),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_timestamp' => time(),
                'oauth_token' => App::$config[static::class]['token'],
                'oauth_version' => '1.0',
            ];
        } catch (Exception $e) {
            throw new RuntimeException('Unable to generate oauth nonce');
        }
    }

    private function getSignature($method, $url, $params): string
    {
        ksort($params);

        $signBase = implode(
            '&',
            [
                strtoupper($method),
                rawurlencode($url),
                rawurlencode(
                    http_build_query($params, '', '&', PHP_QUERY_RFC3986)
                ),
            ]
        );

        $signingKey = App::$config[static::class]['consumer_secret'] . '&' . App::$config[static::class]['token_secret'];

        return base64_encode(hash_hmac('SHA1', $signBase, $signingKey, true));
    }

    private function getAuthorizationHeader($params): string
    {
        $data = [];

        foreach ($params as $key => $value) {
            if (strpos($key, 'oauth_') === 0) {
                $data[] = "{$key}=\"" . rawurlencode($value) . "\"";
            }
        }

        return 'OAuth ' . implode(', ', $data);
    }
}
